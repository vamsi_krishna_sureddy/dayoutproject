//
//  LoadingDetailsThreadVC.swift
//  DayOut
//
//  Created by Student on 4/23/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit

class LoadingDetailsThreadVC: UIViewController {
    
    var attraction:Attraction!
    
    var appDelegate:AppDelegate!
    
    var fromTVC:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = (UIApplication.sharedApplication().delegate) as AppDelegate
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        if (fromTVC != false){
            self.performSegueWithIdentifier("showDetailsSegue", sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showDetailsSegue" {
            self.fromTVC = false
            var detailVC:DetailsVC = segue.destinationViewController as DetailsVC
            detailVC.attraction = self.attraction
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
