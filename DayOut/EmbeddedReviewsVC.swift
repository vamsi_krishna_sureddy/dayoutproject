//
//  EmbeddedReviewsVC.swift
//  DayOut
//
//  Created by Student on 4/22/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit

class EmbeddedReviewsVC: UIViewController {

    var attraction:Attraction!
    
    var hasReviews:Bool!
    
  //  @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var reviewsWebVIew: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var reviews = attraction.attractionDetails.resultDictionary["reviews"]
        if reviews == nil{
            hasReviews = false
        }else {
            hasReviews = true
        }
        self.loadReviews()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadReviews(){
        if(!hasReviews){
            reviewsWebVIew.alpha = 0.5
            reviewsWebVIew.loadHTMLString("No Reviews Found", baseURL: nil)
            //textView.alpha = 0.5
        }else{
            reviewsWebVIew.alpha = 1
            //textView.alpha = 1
            var textString:String = ""
            var reviewsArray:NSArray = attraction.attractionDetails.resultDictionary["reviews"] as NSArray
            var label:UILabel!
            for review in reviewsArray {
                var currentReviewDictionary:NSDictionary = review as NSDictionary
                var authorName = currentReviewDictionary["author_name"] as String
                var text:String = currentReviewDictionary["text"] as String
                
//                var label:UILabel = self.viewWithTag(2000) as UILabel
//                var authorView:UIView = textView.viewWithTag(3000)! as UIView
//                
//                label.text = authorName
//                authorView.addSubview(label)
//                
//                textView.addSubview(authorView)
            //    var rating = currentReviewDictionary["rating"] as String

                textString = textString.stringByAppendingString("<span style=\"color:DarkRed\">\(authorName)</span>&nbsp&nbsp\(text)")
//                textString = textString.stringByAppendingString("\n-- ")
//                textString = textString.stringByAppendingString(authorName)
                textString = textString.stringByAppendingString("\n\n__________________________________________________________________________________________\n\n")
                
//                var targetString = textString
//                var attributedString:NSMutableAttributedString = NSMutableAttributedString(string: targetString)
//                var stringToBold = authorName
//                var boldRange:NSRange = targetString.rangeOfString(stringToBold, options: nil, range: nil, locale: nil)
//                NSString *yourString = @"This is to be bold. This is normal string.";
//                NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithAttributedString:yourString];
//                NSString *boldString = @"This is to be bold";
//                NSRange boldRange = [yourString rangeOfString:boldString];
//                [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont boldSystemFontOfSize:12] range:boldRange];
//                [yourLabel setAttributedText: yourAttributedString];
                
                //textView.text = textString
                reviewsWebVIew.loadHTMLString(textString, baseURL: nil)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
