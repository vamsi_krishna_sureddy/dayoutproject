//
//  Attraction.swift
//  DayOut
//
//  Created by Student on 4/14/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit

class Attraction: NSObject {
    var category:NSArray
    //var budget:Int
    var ratings:Double
    var distance:Int
    var image:UIImage
    var attractionDetails:AttractionDetail
//    var location:String
    var name:String
//    var contact:String
    var latitude:Double
    var logitude:Double
    
    init(category:NSArray, ratings:Double, distance:Int, image:UIImage, attractionDetails:AttractionDetail, name:String, latitude:Double, logitude:Double){
        self.category = category
       // self.budget = budget
        self.ratings = ratings
        self.distance = distance
        self.image = image
        self.attractionDetails = attractionDetails
      //  self.location = location
        self.name = name
    //    self.contact = contact
        self.latitude = latitude
        self.logitude = logitude
    }
}
