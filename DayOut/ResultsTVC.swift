//
//  ResultsTVC.swift
//  DayOut
//
//  Created by Student on 4/14/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreLocation
import QuartzCore
class ResultsTVC: UITableViewController, UITextFieldDelegate {
    
    let imageTag:Int = 100
    let mailLabelTag:Int = 200
    let distanceTag:Int = 300
    let budgetTag:Int = 400
    //let button:Int = 25
    
    let numberOfRatingsTag = 1200
    
    let ratingsLabelTag:Int = 500
    
    let descriptionLabelTag:Int = 1100
    
    let priceLabel1Tag:Int = 1000
    let priceLabel2Tag:Int = 600
    let priceLabel3Tag:Int = 700
    let priceLabel4Tag:Int = 800
   // let priceLabel5Tag:Int = 900
    
    let maxWidth = 400
    let imageHeight = 400
    
    let restaurantsSection = 0
    let nightLifeSection = 1
    let shoppingMallsSection = 2
    let entertainmentSection = 3
    let placesOfWorshipSection = 4
    let localActivitiesSection = 5
    
    var searchZipCode:String!
    
    
    @IBOutlet weak var zipCodeTextField: UITextField!
    
    var searchType:String!
    
   // let appDelegate:AppDelegate!
    
    let sectionHeaders = ["Restaurants","Night Life","Shopping Malls/Leisure","Entertainment","Places of Worship","Local Activities"]
    
   // let typesString = "amusement_park|aquarium|art_gallery|bakery|bar|bowling_alley|cafe|casino|church|city_hall|campground|establishment|food|hindu_temple|movie_theater|museum|night_club|park|place_of_worship|restaurant|rv_park|shopping_mall|spa|zoo|natural_feature|colloquial_area"
    let typesString = "|food|restaurant"
    
    var distance:Int = 16093
    var zipCode:String!
    
    var radius:Int!

    var data = [Attraction]()
    var filteredData = [Attraction]()
    
    var attractions:NSDictionary!
    var queriedAttractions:NSArray!
    
  //  var placeMark:CLPlacemark!
    var latitude:CLLocationDegrees!
    var longitude:CLLocationDegrees!
    
    var restaurents = [Attraction]()
    var nightLife = [Attraction]()
    var shoppingMalls = [Attraction]()
    var entertainment = [Attraction]()
    var placesOfWorship = [Attraction]()
    var localActivities = [Attraction]()
    

    
    let sectionTitles:[String] = [""]
    
    let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
       // ratingsLabel.layer.cornerRadius = 5
     //   self.placeMark = appDelegate.placeMark
        //var tmpButton = self.view.viewWithTag(button) as UIButton
        //tmpButton.frame = CGRectMake(1000, 100, 20, 20)
        
        
        //tmpButton.backgroundColor = UIColor.whiteColor()
        //tmpButton.setTitle("Done", forState: UIControlState.Normal)
       // tmpButton.addTarget(self, action: "buttonAction", forControlEvents: UIControlEvents.TouchUpInside)
        //self.view.addSubview(tmpButton)
        self.latitude = appDelegate.latitude
        self.longitude = appDelegate.longitude
        println("location - \(self.latitude)")
        println("location - \(self.longitude)")
        self.attractions = self.getJson()
        var resultsArray:NSArray = attractions["results"] as NSArray
        println("------------------Results Array----------------------------")
        println(resultsArray)
        var currentAttraction:Attraction!
        for(var i=0;i<resultsArray.count;i++){
            println("------------------Photos Array----------------------------")
            println(resultsArray[i]["photos"])
            var photosArray:NSArray? = resultsArray[i]["photos"] as? NSArray
            var image:UIImage
            if photosArray != nil {
                println("yayyyyyyyy")
                println(photosArray)
                var imageReference = String(photosArray![0]["photo_reference"] as NSString)
                image = getImage(imageReference)
            }else{
                image = UIImage(named: "watermarksmiley.jpg")!
            }
            //var imageReference = String(photosArray[2] as NSString)
            var placeId = resultsArray[i]["place_id"] as NSString
            var placeDetail:AttractionDetail = getPlaceDetail(placeId)
            var geometry:NSDictionary = resultsArray[i]["geometry"] as NSDictionary
            var loc = geometry["location"] as NSDictionary
            var category = resultsArray[i]["types"] as NSArray!
           // var ratings = resultsArray[i]["rating"] as Double
            var ratings = 3.0
            var name = resultsArray[i]["name"] as String
            var lat = loc["lat"] as Double
            var lng = loc["lng"] as Double
            currentAttraction = Attraction(category: category, ratings: ratings , distance: self.distance, image: image, attractionDetails: placeDetail, name: name, latitude: lat, logitude: lng)
            println("Name -")
            println(currentAttraction.attractionDetails.resultDictionary["name"] as String)
            println("Ratings - ")
            var priceLevel = currentAttraction.attractionDetails.resultDictionary["price_level"]
            if priceLevel != nil {
                println(currentAttraction.attractionDetails.resultDictionary["price_level"] as Double)
            }else{
                println("Ratings not available")
            }
            
            data.append(currentAttraction)
        }
//        func buttonAction(sender:UIButton!)
//        {
//            println("Button Tapped")
//        }
        
        func viewWillAppear(animated: Bool){
            println("Inside rootTVC viewWillAppear")
            //self.navigationItem.hidesBackButton=true
            //self.navigationItem.setHidesBackButton(true, animated: true)
            //self.navigationItem.setHidesBackButton(true,animated: true)
//            UIBarButtonItem b=UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.Done, target: nil, action: nil);
            
            //self.navigationItem.leftBarButtonItem
            //self.navigationItem.hidesBackButton=true;
            var button=UIBarButtonItem()
            button.title=""
            self.navigationItem.leftBarButtonItem=button
        }
        
       // self.sortDataIntoSections()
       
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        println("Number of sections - \(self.restaurents.count+self.nightLife.count+self.shoppingMalls.count+self.entertainment.count+self.placesOfWorship.count+self.localActivities.count)")
        return 1
    }
    
    
//    @IBAction func saveAttraction(sender: UIButton) {
//        sender.backgroundColor = UIColor.greenColor()
//        sender.alpha = 1
//        sender.setTitle("Saved!", forState: UIControlState.Selected)
////        sender.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Highlighted)
//    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        var zipCode:String! = textField.text
       // println(zipCode)
        self.searchZipCode = zipCode
       // println(searchZipCode)
        self.performSegueWithIdentifier("loadingNewLocationSegue", sender: self)
        return false
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        var rowsInSection:Int!
//        if tableView == self.searchDisplayController!.searchResultsTableView {
//            return self.filteredData.count
//        } else {
           // println("Section - \(section)")
//            switch(section){
//                case self.restaurantsSection: rowsInSection = self.restaurents.count
//                case self.nightLifeSection: rowsInSection = self.nightLife.count
//                case self.shoppingMallsSection: rowsInSection = self.shoppingMalls.count
//                case self.entertainmentSection: rowsInSection = self.entertainment.count
//                case self.placesOfWorshipSection:
//                  //  println("Places Of worship count - \(self.placesOfWorship.count)")
//                    rowsInSection = self.placesOfWorship.count
//                case self.localActivitiesSection:
//                rowsInSection = self.localActivities.count
//                default: rowsInSection = 0
//           // }
//        }
//        println("Section - \(section) rowsInSection - \(rowsInSection)")
        if data.count == 0 {
            return 1
        }
        return data.count
    }
    
//    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return self.sectionHeaders[section]
//    }

    
//    @IBAction func unWindToHome(sender: UIButton) {
//        
//        performSegueWithIdentifier("unwindToHomeSegue", sender: self)
//        
//    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        var attraction:Attraction!
        
        
        if data.count != 0 {
            attraction = self.data[indexPath.row]
            if self.searchType == "restaurant" || self.searchType == "night_club" {
                cell = self.tableView.dequeueReusableCellWithIdentifier("restaurantsCell") as UITableViewCell
                var priceLabel1:UILabel? = (cell.viewWithTag(priceLabel1Tag) as UILabel)
                var priceLabel2:UILabel? = (cell.viewWithTag(priceLabel2Tag) as UILabel)
                var priceLabel3:UILabel? = (cell.viewWithTag(priceLabel3Tag) as UILabel)
                var priceLabel4:UILabel? = (cell.viewWithTag(priceLabel4Tag) as UILabel)
              //  var priceLabel5:UILabel? = (cell.viewWithTag(priceLabel5Tag) as UILabel)
                
                var priceLabelArray = [UILabel]()
                
                priceLabelArray.append(priceLabel1!)
                priceLabelArray.append(priceLabel2!)
                priceLabelArray.append(priceLabel3!)
                priceLabelArray.append(priceLabel4!)
              //  priceLabelArray.append(priceLabel5!)
                
                var priceLevel:Int!
                if attraction.attractionDetails.resultDictionary["price_level"] != nil {
                    priceLevel = attraction.attractionDetails.resultDictionary["price_level"] as Int
                }else{
                    priceLevel = 0
                }
                
                
                for(var i=0;i<priceLevel;i++){
                    priceLabelArray[i].alpha = 1
                }
                
                if priceLevel != 0{
                    for(var i=3;i>=priceLevel;i--){
                        priceLabelArray[i].alpha = 0.5
                    }
                }
                
            }else if self.searchType == "shopping_mall" || self.searchType == "church" {
                cell = self.tableView.dequeueReusableCellWithIdentifier("shoppingMallCell") as UITableViewCell
            }
            
            
            
            var mainLabel:UILabel? = (cell.viewWithTag(mailLabelTag) as UILabel)
            var photo:UIImageView? = (cell.viewWithTag(imageTag) as UIImageView)
            var ratingsLabel:UILabel? = (cell.viewWithTag(ratingsLabelTag) as UILabel)
            var descriptionLabel:UITextView = (cell.viewWithTag(descriptionLabelTag) as UITextView)
            var numberOfRatingsLabel:UILabel = (cell.viewWithTag(numberOfRatingsTag) as UILabel)
            var distanceLabel:UILabel = (cell.viewWithTag(1300) as UILabel)
            
            var numberOfRatingsText:String!
            
            if attraction.attractionDetails.resultDictionary["user_ratings_total"] != nil {
                numberOfRatingsText = (attraction.attractionDetails.resultDictionary["user_ratings_total"] as Int).description
            }else{
                numberOfRatingsText = "0"
            }
            
            var sourceLocation:CLLocation = CLLocation(latitude: appDelegate.latitude, longitude: appDelegate.longitude)
            var desinationLocation:CLLocation = CLLocation(latitude: attraction.latitude, longitude: attraction.logitude)
          //  var computedValue = (sourceLocation.distanceFromLocation(desinationLocation) * 0.000621371)
            
            var calculatedDistance:Double = sourceLocation.distanceFromLocation(desinationLocation) * 0.000621371
            
            //distanceLabel.text = "\(calculatedDistance) mi"
            distanceLabel.text = String(format:"%.1f mi",calculatedDistance)
            numberOfRatingsLabel.text = "(\(numberOfRatingsText))"
            mainLabel!.text = attraction.name
            //   distanceLabel!.text = String(attraction.distance)
            //  budgetLabel!.text = "100"
            photo!.image = attraction.image
            if attraction.attractionDetails.resultDictionary["rating"] != nil {
                ratingsLabel?.text = (attraction.attractionDetails.resultDictionary["rating"] as Double).description
            }else{
                ratingsLabel?.text = "--"
                // ratingsLabel?.backgroundColor = UIColor.lightGrayColor()
            }
            if attraction.attractionDetails.resultDictionary["reviews"] != nil {
                var currentAttractionReviews:NSArray = attraction.attractionDetails.resultDictionary["reviews"] as NSArray
                
                if currentAttractionReviews.count != 0 {
                    descriptionLabel.text = (currentAttractionReviews[0]["text"]) as String
                }else {
                    descriptionLabel.text = ""
                }
            }else {
                descriptionLabel.text = ""
            }
            
            // descriptionLabel.text =
            //ratingsLabel?.layer.cornerRadius = 10
            
        }else if data.count == 0{
            cell = self.tableView.dequeueReusableCellWithIdentifier("noResultsCell") as UITableViewCell
            var noResultsCell:UILabel? = (cell.viewWithTag(4500) as UILabel)
            noResultsCell!.text = "No Results Found"
            noResultsCell!.alpha = 0.7
        }
        
        return cell
    }
    
    func filterContentForSearchText(searchText: String) {
        // Filter the array using the filter method
        self.filteredData = self.data.filter({( attraction : Attraction) -> Bool in
          //  let categoryMatch = (attr.category == scope)
            let stringMatch = attraction.name.rangeOfString(searchText)
            return (stringMatch != nil)
        })
    }
    
//    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
//        self.filterContentForSearchText(searchString)
//        return true
//    }
//    
//    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
//        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text)
//        return true
//    }
    
    func getJson() -> NSDictionary{
        let latitude = self.latitude
        let longitude = self.longitude
        
        var urlstr:String = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=\(self.distance)&types=\(self.searchType)&key=\(appDelegate.APP_KEY)"
        println("Main json - \(urlstr)")
        var request:NSURLRequest = NSURLRequest(URL: NSURL(string:urlstr)!)
        var error:NSError?
        var results = NSURLConnection.sendSynchronousRequest(request,returningResponse: nil, error:&error)!
        var attractions = NSJSONSerialization.JSONObjectWithData(results,options:NSJSONReadingOptions.AllowFragments, error: &error) as NSDictionary
       // println(attractions)
        return attractions
    }
    
    func getImage(imageReference:String) -> UIImage{
        var urlstr:String = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=\(self.maxWidth)&photoreference=\(imageReference)&key=\(appDelegate.APP_KEY)"
        var image:UIImage
        println("Image url - \(urlstr)")
        let url = NSURL(string: urlstr)
        let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
        if data != nil{
            image = UIImage(data: data!)!
        }else{
            image = UIImage(named: "smiley.jpg")!
        }
        
        return image
//        println("------------------Image Search URL----------------------------")
//        println(urlstr)
//        var request:NSURLRequest = NSURLRequest(URL: NSURL(string:urlstr)!)
//        var error:NSError?
//        var results = NSURLConnection.sendSynchronousRequest(request,returningResponse: nil, error:&error)!
//        var image = NSJSONSerialization.JSONObjectWithData(results,options:NSJSONReadingOptions.AllowFragments, error: &error) as UIImage
//        return image
    }
    
    func sortDataIntoSections(){
        var sequence:[String]!
        for attraction in self.data {
            sequence = attraction.category as [String]
            if (contains(sequence,"food") || contains(sequence,"bakery") || contains(sequence,"bar") || contains(sequence,"cafe")){
                self.restaurents.append(attraction)
            }
            
            if (contains(sequence,"bar") || contains(sequence,"casino") || contains(sequence,"night_club")){
                self.nightLife.append(attraction)
            }
            
            if (contains(sequence,"shopping_mall") || contains(sequence,"spa")){
                self.shoppingMalls.append(attraction)
            }
            
            if (contains(sequence,"casino") || contains(sequence,"amusement_park") || contains(sequence,"movie_theater") || contains(sequence,"park") || contains(sequence,"rv_park") || contains(sequence,"bowling_alley") || contains(sequence,"campground")){
                self.entertainment.append(attraction)
            }
            
            if (contains(sequence,"hindu_temple") || contains(sequence,"place_of_worship") || contains(sequence,"church")){
                self.placesOfWorship.append(attraction)
            }
            
            if (contains(sequence,"aquarium") || contains(sequence,"art_gallery") || contains(sequence,"city_hall") || contains(sequence,"zoo") || contains(sequence,"natural_feature") || contains(sequence,"museum")){
                self.localActivities.append(attraction)
            }
        }
    }
    
    func getPlaceDetail(placeId:String) -> AttractionDetail {
        var urlstr:String = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeId)&key=\(appDelegate.APP_KEY)"
        println(urlstr)
        var request:NSURLRequest = NSURLRequest(URL: NSURL(string:urlstr)!)
        var error:NSError?
        var results = NSURLConnection.sendSynchronousRequest(request,returningResponse: nil, error:&error)!
        var detailContent:NSDictionary = NSJSONSerialization.JSONObjectWithData(results,options:NSJSONReadingOptions.AllowFragments, error: &error) as NSDictionary
        var resultsDictionary:NSDictionary?
        if detailContent["result"] == nil {
            resultsDictionary = NSDictionary()
        }else{
            resultsDictionary = detailContent["result"] as NSDictionary
        }
//        var hours:NSArray?
//        var status:Bool?
//        var openHoursArray:NSDictionary? = resultsDictionary["opening_hours"] as? NSDictionary
//        
//        if openHoursArray != nil {
//            hours = openHoursArray!["weekday_text"] as? NSArray
//            status = openHoursArray!["open_now"] as? Bool
//        }else{
//            hours = ["empty"]
//            status = false
//        }
//
//        var reviewsArray:NSArray? = resultsDictionary["reviews"] as NSArray
//        var reviews:NSArray?
//        if reviewsArray != nil {
//            reviews = reviewsArray
//        }else{
//            reviews = ["empty"]
//        }
//        
//        var websiteUrl = resultsDictionary["website"] as String
//        var website:String?
//        if websiteUrl != nil{
//            website = websiteUrl
//        }
        var attractionDetail:AttractionDetail = AttractionDetail(adress: resultsDictionary!["formatted_address"] as String, resultDictionary: resultsDictionary!)
        
        return attractionDetail
    }
    
    
    @IBAction func unwindToTVC(segue:UIStoryboardSegue) {
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "detailsViewSegue" ||  segue.identifier == "shoppingAndPlacesOfWorshipSegue"{
            var detailVC:LoadingDetailsThreadVC = segue.destinationViewController as LoadingDetailsThreadVC
            var selectedRow = tableView.indexPathForSelectedRow()!.row
            var selectedAttraction = self.data[selectedRow]
            detailVC.attraction = selectedAttraction
            detailVC.fromTVC = true
           // detailVC.navigationItem.title = selectedAttraction.name
        }
        
        if segue.identifier == "loadingNewLocationSegue" {
            var loadingVC = segue.destinationViewController as LoadingThreadVC
            loadingVC.zipCode = self.searchZipCode
            loadingVC.shouldUpdate = true
            loadingVC.searchType = self.searchType
        }
    }
    

}
