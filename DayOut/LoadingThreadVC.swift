//
//  LoadingThreadVC.swift
//  DayOut
//
//  Created by Student on 4/22/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreLocation
class LoadingThreadVC: UIViewController {

    var searchType:String!
    
    var zipCode:String?
    
    var latitude:CLLocationDegrees?
    var longitude:CLLocationDegrees?
    
    var shouldUpdate:Bool = false
    
    var appDelegate:AppDelegate!
    
    @IBOutlet weak var searchTypeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = (UIApplication.sharedApplication().delegate) as AppDelegate
        searchTypeLabel.text = "\(self.searchType)"
        if shouldUpdate {
            self.updateLocation()
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.performSegueWithIdentifier("showResultsSegue", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateLocation(){
        var urlstr:String = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(zipCode!)"
        println("New Location URL - \(urlstr)")
        var request:NSURLRequest = NSURLRequest(URL: NSURL(string:urlstr)!)
        var error:NSError?
        var results = NSURLConnection.sendSynchronousRequest(request,returningResponse: nil, error:&error)!
        var locationDetails = NSJSONSerialization.JSONObjectWithData(results,options:NSJSONReadingOptions.AllowFragments, error: &error) as NSDictionary
        var resultsArray:NSArray! = locationDetails["results"] as NSArray
        var geometry:NSDictionary = resultsArray[0]["geometry"] as NSDictionary
        var locDictionary:NSDictionary = geometry["location"] as NSDictionary
        appDelegate.latitude = locDictionary["lat"] as CLLocationDegrees
        appDelegate.longitude = locDictionary["lng"] as CLLocationDegrees
        
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showResultsSegue" {
            var destinationTVC:ResultsTVC = segue.destinationViewController as ResultsTVC
            destinationTVC.searchType = self.searchType
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
