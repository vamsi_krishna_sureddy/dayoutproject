//
//  PagedScrollViewController.swift
//  DayOut
//
//  Created by Student on 4/23/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit

class PagedScrollViewController: UIViewController, UIScrollViewDelegate {

    var attraction:Attraction!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    
    var pageImages: [UIImage] = []
    var pageViews: [UIImageView?] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if attraction.attractionDetails.resultDictionary["photos"] != nil {
//            var photoArray:NSArray = attraction.attractionDetails.resultDictionary["photos"] as NSArray
        
//            pageImages = [UIImage(named: "worship.png")!,
//                UIImage(named: "smiley.jpg")!,
//                UIImage(named: "nightclubs.jpg")!]
            loadPhotosArray()
        
            let pageCount = pageImages.count
            
            // 2
            pageControl.currentPage = 0
            pageControl.numberOfPages = pageCount
            
            // 3
            for _ in 0..<pageCount {
                pageViews.append(nil)
            }
            
            // 4
            let pagesScrollViewSize = scrollView.frame.size
            scrollView.contentSize = CGSize(width: pagesScrollViewSize.width * CGFloat(pageImages.count),
                height: pagesScrollViewSize.height)
            
            // 5
            loadVisiblePages()
        //}
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadPhotosArray(){
        if attraction.attractionDetails.resultDictionary["photos"] != nil{
            var photoArray:NSArray = attraction.attractionDetails.resultDictionary["photos"] as NSArray
            var count = 3
            if photoArray.count < count {
                count = photoArray.count
            }
            for(var i=0;i<count;i++) {
                var attributeDictionary:NSDictionary = photoArray[i] as NSDictionary
                var photoReference:String = attributeDictionary["photo_reference"] as String
                var width:Int = attributeDictionary["width"] as Int
                println(pageImages.count)
                self.pageImages.append(self.getImage(photoReference, maxWidth: width))
              //  self.pageImages[i] = self.getImage(photoReference, maxWidth: width)
            }
        }else{
            self.pageImages = [UIImage(named: "smiley.jpg")!]
        }
        
    }
    
    func getImage(imageReference:String, maxWidth:Int) -> UIImage{
        var urlstr:String = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=\(maxWidth)&photoreference=\(imageReference)&key=\(appDelegate.APP_KEY)"
        var image:UIImage
        println("Image url inside scrollview - \(urlstr)")
        let url = NSURL(string: urlstr)
        let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
        if data != nil{
            image = UIImage(data: data!)!
        }else{
            image = UIImage(named: "smiley.jpg")!
        }
        
        return image
        //        println("------------------Image Search URL----------------------------")
        //        println(urlstr)
        //        var request:NSURLRequest = NSURLRequest(URL: NSURL(string:urlstr)!)
        //        var error:NSError?
        //        var results = NSURLConnection.sendSynchronousRequest(request,returningResponse: nil, error:&error)!
        //        var image = NSJSONSerialization.JSONObjectWithData(results,options:NSJSONReadingOptions.AllowFragments, error: &error) as UIImage
        //        return image
    }
    
    func loadPage(page: Int) {
        if page < 0 || page >= pageImages.count {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        
        // 1
        if let pageView = pageViews[page] {
            // Do nothing. The view is already loaded.
        } else {
            // 2
            var frame = scrollView.bounds
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0.0
            
            // 3
            let newPageView = UIImageView(image: pageImages[page])
            newPageView.contentMode = .ScaleAspectFit
            newPageView.frame = frame
            scrollView.addSubview(newPageView)
            
            // 4
            pageViews[page] = newPageView
        }
    }
    
    func purgePage(page: Int) {
        if page < 0 || page >= pageImages.count {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        
        // Remove a page from the scroll view and reset the container array
        if let pageView = pageViews[page] {
            pageView.removeFromSuperview()
            pageViews[page] = nil
        }
    }
    
    func loadVisiblePages() {
        // First, determine which page is currently visible
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
        
        // Update the page control
        pageControl.currentPage = page
        
        // Work out which pages you want to load
        let firstPage = page - 1
        let lastPage = page + 1
        
        // Purge anything before the first page
        for var index = 0; index < firstPage; ++index {
            purgePage(index)
        }
        
        // Load pages in our range
        for index in firstPage...lastPage {
            loadPage(index)
        }
        
        // Purge anything after the last page
        for var index = lastPage+1; index < pageImages.count; ++index {
            purgePage(index)
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView!) {
        // Load the pages that are now on screen
        loadVisiblePages()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
