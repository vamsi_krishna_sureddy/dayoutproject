//
//  DetailsVC.swift
//  DayOut
//
//  Created by Student on 4/14/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {

    var attraction:Attraction!
    
    @IBOutlet weak var detailImage: UIImageView!
    
    @IBOutlet weak var detailTextLabel: UITextView!
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    @IBOutlet weak var mapKitContainerVC: UIView!
   
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var detailsContainerVC: UIView!
    
    @IBOutlet weak var reviewsContainer: UIView!
    
    @IBOutlet weak var photosVikewContainerWithImages: UIView!
    
    @IBOutlet weak var photosViewContainerWithoutImages: UIView!
    
    override func viewDidLoad() {
        
      //  var photo:UIImageView? = (cell.viewWithTag(detailImageTag) as UIImageView)
    //    photo!.image = attraction.image
      //  (self.navigationBar as UINavigationItem).title = attraction.name
        self.navigationBar.topItem?.title = attraction.name
        self.mapKitContainerVC.hidden = true
        self.detailsContainerVC.hidden = false
        self.reviewsContainer.hidden = false
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func indexChanged(sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            self.mapKitContainerVC.hidden = true
            self.detailsContainerVC.hidden = false
            self.reviewsContainer.hidden = false
        case 1:
            self.mapKitContainerVC.hidden = false
            self.detailsContainerVC.hidden = true
            self.reviewsContainer.hidden = true
        default:
            break; 
        }
    }

    
    @IBAction func backToTVC(sender: UIBarButtonItem) {
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "embeddedSimilarPlacesSegue" {
            var detailVC = segue.destinationViewController as EmbeddedDetailsTVC
            detailVC.attraction = self.attraction
            // detailVC.navigationItem.title = selectedAttraction.name
        }else if segue.identifier == "embeddedDetailsViewSegue" {
            var detailVC = segue.destinationViewController as EmbeddedDetailsVC
            detailVC.attraction = self.attraction
            // detailVC.navigationItem.title = selectedAttraction.name
        }else if segue.identifier == "embeddedReviewsSegue" {
            var detailVC = segue.destinationViewController as EmbeddedReviewsVC
            detailVC.attraction = self.attraction
        }else if segue.identifier == "embeddedMapsSegue" {
            var detailVC = segue.destinationViewController as EmbeddedMapKitVC
            detailVC.attraction = self.attraction
        }else if segue.identifier == "scrollViewIdentifier" {
            var detailVC = segue.destinationViewController as PagedScrollViewController
            detailVC.attraction = self.attraction
        }
        
    }
    

}
