//
//  EmbeddedDetailsVC.swift
//  DayOut
//
//  Created by Student on 4/15/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit

class EmbeddedDetailsVC: UIViewController {

    var attraction:Attraction!
    
    @IBOutlet weak var PlaceTitle: UILabel!
    
    @IBOutlet weak var adressTextArea: UITextView!
    
    @IBOutlet weak var openHoursTextArea: UITextView!
    
    @IBOutlet weak var openHoursStatusLabel: UILabel!
    
   // @IBOutlet weak var image: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.PlaceTitle.text = attraction.name
        var adressArray:[String] = (attraction.attractionDetails.adress).componentsSeparatedByString(",")
        self.adressTextArea.text = "\(adressArray[0].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n\(adressArray[1].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n\(adressArray[2].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n\(adressArray[3].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n"
        
        if attraction.attractionDetails.resultDictionary["opening_hours"] != nil {
            var openingHoursDictionary:NSDictionary = attraction.attractionDetails.resultDictionary["opening_hours"] as NSDictionary
            if (openingHoursDictionary["open_now"] as Bool){
                self.openHoursStatusLabel.text = "Open Now!"
                openHoursStatusLabel.textColor = UIColor.greenColor()
            }else{
                self.openHoursStatusLabel.text = "Closed Now"
                openHoursStatusLabel.textColor = UIColor.redColor()
            }
            // self.openHoursStatusLabel.text = (openingHoursDictionary["open_now"] as Bool).description
            
            //        if openHoursStatusLabel.text == "false" {
            //            openHoursStatusLabel.textColor = UIColor.redColor()
            //        }else{
            //            openHoursStatusLabel.textColor = UIColor.greenColor()
            //        }
            
            var weekendHoursArray = openingHoursDictionary["weekday_text"] as NSArray
            
            self.openHoursTextArea.text = "\(weekendHoursArray[0].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n\(weekendHoursArray[1].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n\(weekendHoursArray[2].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n\(weekendHoursArray[3].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n\(weekendHoursArray[4].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n\(weekendHoursArray[5].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n\(weekendHoursArray[6].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))\n"
        }else{
            self.openHoursTextArea.text = "Not Available"
            self.openHoursTextArea.alpha = 0.5
            self.openHoursStatusLabel.text = ""
        }
                //  self.image.image = attraction.image
       // self.textArea.text = attraction.attractionDescription
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
