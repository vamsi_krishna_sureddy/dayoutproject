//
//  EmbeddedMapKitVC.swift
//  DayOut
//
//  Created by Student on 4/23/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import MapKit
class EmbeddedMapKitVC: UIViewController {

    var attraction:Attraction!
    var appDelegate:AppDelegate!
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        var geometryDictionary:NSDictionary = attraction.attractionDetails.resultDictionary["geometry"] as NSDictionary
        var locationDetails:NSDictionary = geometryDictionary["location"] as NSDictionary
        var latitude:CLLocationDegrees = locationDetails["lat"] as CLLocationDegrees
        var longitude:CLLocationDegrees = locationDetails["lng"] as CLLocationDegrees
        println(latitude)
        println(longitude)
        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        let pinView = MKPointAnnotation()
        pinView.setCoordinate(location)
        pinView.title = attraction.name
       // pinView.subtitle = "resghrd"
        mapView.addAnnotation(pinView)
//        pinView.subtitle = "Rati"
      // mapView.userLocation.coordinate.
        
//        let location2 = CLLocationCoordinate2D(latitude: appDelegate.latitude, longitude: appDelegate.longitude)
//        
//        let region2 = MKCoordinateRegion(center: location2, span: span)
//        
//        let pinView2 = MKPointAnnotation()
//        pinView2.setCoordinate(location)
//        pinView2.title = "YOU ARE HERE"
//        
//        
//        mapView.setRegion(region2, animated: true)
//     //   mapView.addAnnotation(pinView)
//        mapView.addAnnotation(pinView2)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
