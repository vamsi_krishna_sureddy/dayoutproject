//
//  AppDelegate.swift
//  DayOut
//
//  Created by Student on 4/14/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreLocation
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,  CLLocationManagerDelegate {

    var window: UIWindow?
    var attractions:NSArray!
    let APP_KEY = "AIzaSyC0RMdOXKuQZDoGnu4j6MNC15IhkOna4RM" //master key
    //let APP_KEY = "AIzaSyAE5eKji6FzL4TkPLBwPrI73ELMc7WYhvs" //Karunakar
   // let APP_KEY = "AIzaSyBvTFrjnSsE6EGz9-knC-2RuZl_uVaDPZA"
    //let APP_KEY = "AIzaSyAxOeYE_jFslmKXZnSsLjua3X3ke8DaPkE"
   
   // let APP_KEY = "AIzaSyBE_WXqJ-XeyKwvSU77aUkSHxzE-EbrGxo"
    
   // let APP_KEY = "AIzaSyC0RMdOXKuQZDoGnu4j6MNC15IhkOna4RM"
    
    var locManager = CLLocationManager()
    
    var locationStatus : NSString = "Not Started"
    
    
    var placeMark:CLPlacemark!
    
    var latitude:CLLocationDegrees!
    
    var longitude:CLLocationDegrees!
    
    //var zipCode:String!

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
       // locManager.requestAlwaysAuthorization()
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
        
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil {
                println("Reverse geocoder failed with error" + error.localizedDescription)
                //return
            }
            
            if placemarks.count > 0 {
                let pm = placemarks[0] as CLPlacemark
                self.displayLocationInfo(pm)
            }
            else {
                println("Problem with the data received from geocoder")
            }
        })
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        //stop updating location to save battery life
        
        locManager.stopUpdatingLocation()
        println(placemark.location.coordinate.latitude)
        println(placemark.location.coordinate.longitude)
        println(placemark.locality)
        
        self.placeMark = placemark
        self.latitude = placemark.location.coordinate.latitude
        self.longitude = placemark.location.coordinate.longitude
        
        
      //  self.zipCodeTextField.text = placeMark.postalCode
        
    }
    
//    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
//        
//        var alert = UIAlertController(title: "Alert", message: "WIFI Off", preferredStyle: .Alert)
//        
//        var OKAction = UIAlertAction(title: "OK", style: .Default ,handler: nil)
//        alert.addAction(OKAction)
//        
//        self.presentViewController(alert, animated: true, completion:nil)
//        
//    }
    
    // authorization status
    func locationManager(manager: CLLocationManager!,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var shouldIAllow = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access to location"
            case CLAuthorizationStatus.Denied:
                locationStatus = "User denied access to location"
                //            case CLAuthorizationStatus.NotDetermined:
                //                locationStatus = "Status not determined"
            default:
                locationStatus = "Allowed to location Access"
                shouldIAllow = true
            }
            //            NSNotificationCenter.defaultCenter().postNotificationName("LabelHasbeenUpdated", object: nil)
            if (shouldIAllow == true) {
                NSLog("Location to Allowed")
                // Start location services
                locManager.startUpdatingLocation()
            } else {
                NSLog("Denied access: \(locationStatus)")
            }
    }


}

