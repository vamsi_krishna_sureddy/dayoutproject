//
//  RootViewController.swift
//  DayOut
//
//  Created by Student on 4/14/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreLocation
class RootVC: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var distanceTextField: UITextField!
    
    @IBOutlet weak var zipCodeTextField: UITextField!
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var locManager = CLLocationManager()
    
    var ZipCode:String!
    
    var locationStatus : NSString = "Not Started"
    
    var placeMark:CLPlacemark!
       
    override func viewDidLoad() {
        super.viewDidLoad()
  //activityIndicatorView.
        
//        var currentLocation = CLLocation.self
//        
//        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedWhenInUse){
//                
//                currentLocation = locManager.location.
//                
//        }
        
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestAlwaysAuthorization()
       // locManager.startUpdatingLocation()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(animated: Bool) {
        activityIndicatorView.stopAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
        
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil {
                println("Reverse geocoder failed with error" + error.localizedDescription)
                //return
            }
            
            if placemarks.count > 0 {
                let pm = placemarks[0] as CLPlacemark
                self.displayLocationInfo(pm)
            }
            else {
                println("Problem with the data received from geocoder")
            }
        })
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        //stop updating location to save battery life
        
        locManager.stopUpdatingLocation()
        println(placemark.location.coordinate.latitude)
        println(placemark.location.coordinate.longitude)
        println(placemark.locality)

        self.placeMark = placemark
        self.zipCodeTextField.text = placeMark.postalCode
        
        
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        
        var alert = UIAlertController(title: "Alert", message: "WIFI Off", preferredStyle: .Alert)
        
        var OKAction = UIAlertAction(title: "OK", style: .Default ,handler: nil)
        alert.addAction(OKAction)
        
        self.presentViewController(alert, animated: true, completion:nil)
        
    }
    
    // authorization status
    func locationManager(manager: CLLocationManager!,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var shouldIAllow = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access to location"
            case CLAuthorizationStatus.Denied:
                locationStatus = "User denied access to location"
                //            case CLAuthorizationStatus.NotDetermined:
                //                locationStatus = "Status not determined"
            default:
                locationStatus = "Allowed to location Access"
                shouldIAllow = true
            }
//            NSNotificationCenter.defaultCenter().postNotificationName("LabelHasbeenUpdated", object: nil)
            if (shouldIAllow == true) {
                NSLog("Location to Allowed")
                // Start location services
                locManager.startUpdatingLocation()
            } else {
                NSLog("Denied access: \(locationStatus)")
            }
    }


    @IBAction func showActivityIndicator(sender: UIButton) {
        activityIndicatorView.startAnimating()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "searchAttractionsSegue" {
            var destinationTVC = segue.destinationViewController as RootTVC
           // var selectedAttraction = self.data[selectedRow]
         //   destinationTVC.distance = distanceTextField.text.toInt()
         //   destinationTVC.zipCode = zipCodeTextField.text.toInt()
        //    destinationTVC.placeMark = self.placeMark
            // detailVC.navigationItem.title = selectedAttraction.name
        }
    }


}
